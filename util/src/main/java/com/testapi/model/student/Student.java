package com.testapi.model.student;
import com.google.common.base.MoreObjects;
import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "INFORMATION")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "age", nullable = false)
    private int age;


//    public Student() {
//
//    }
//
//    public Student(Builder builder) {
//        this.id = builder.id;
//        this.name = builder.name;
//        this.age = builder.age;
//    }
//
//    public static Student.Builder getBuilder(){return new Builder();}
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public int getAge() {
//        return age;
//    }
//
//    public void setAge(int age) {
//        this.age = age;
//    }
//
//
//    public static class Builder {
//        private int id;
//        private String name;
//        private int age;
//
//        public Builder() {
//
//        }
//
//        public Student.Builder iD(int id){
//            this.id = id;
//            return this;
//        }
//
//        public Student.Builder name(String name){
//            this.name = name;
//            return this;
//        }
//
//        public Student.Builder age(int age){
//            this.age = age;
//            return this;
//        }
//
//        public Student build() {
//            Student build = new Student(this);
//            return build;
//        }

//    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Student student = (Student) o;
//        return id == student.id &&
//                age == student.age &&
//                Objects.equals(name, student.name);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(id, name, age);
//    }
//
//    @Override
//    public String toString() {
//        return MoreObjects.toStringHelper(this)
//                .add("id ", id)
//                .add("name", name)
//                .add("age", age)
//                .toString();
//    }
}



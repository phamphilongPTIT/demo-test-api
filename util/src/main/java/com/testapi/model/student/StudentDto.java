package com.testapi.model.student;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import lombok.*;

import javax.persistence.Column;
import java.util.Objects;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class StudentDto {
    private int id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("age")
    private int age;


//    public StudentDto() {
//
//    }
//
//    public StudentDto(Builder builder) {
//        this.id = builder.id;
//        this.name = builder.name;
//        this.age = builder.age;
//    }
//
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public int getAge() {
//        return age;
//    }
//
//    public void setAge(int age) {
//        this.age = age;
//    }
//
//
//    public static class Builder {
//        private int id;
//        private String name;
//        private int age;
//
//        public Builder() {
//
//        }
//
//        public StudentDto.Builder iD(int id){
//            this.id = id;
//            return this;
//        }
//
//        public StudentDto.Builder name(String name){
//            this.name = name;
//            return this;
//        }
//
//        public StudentDto.Builder age(int age){
//            this.age = age;
//            return this;
//        }
//
//        public StudentDto build() {
//            StudentDto build = new StudentDto(this);
//            return build;
//        }
//
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        StudentDto studentDto = (StudentDto) o;
//        return id == studentDto.id &&
//                age == studentDto.age &&
//                Objects.equals(name, studentDto.name);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(id, name, age);
//    }
//
//    @Override
//    public String toString() {
//        return MoreObjects.toStringHelper(this)
//                .add("id ", id)
//                .add("name", name)
//                .add("age", age)
//                .toString();
//    }
}

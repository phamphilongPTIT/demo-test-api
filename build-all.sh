cd microservice/composite/student-composite-service
docker build -t composite-service:0.1 .

cd microservice/core/student
docker build -t core-service:0.1 .

cd microservice/support/discovery-services
docker build -t discovery-services:0.1 .

cd microservice/support/edge-service
docker build -t api-gateway:0.1 .

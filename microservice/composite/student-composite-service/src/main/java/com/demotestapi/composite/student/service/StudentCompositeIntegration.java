package com.demotestapi.composite.student.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.testapi.model.student.StudentDto;
import com.testapi.model.util.StudentServiceUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

@Component
public class StudentCompositeIntegration {
    private static final Logger LOG = LoggerFactory.getLogger(StudentCompositeIntegration.class);
    private final StudentServiceUtils util;
    private final RestTemplate restOperation;

    @Autowired
    public StudentCompositeIntegration(
            StudentServiceUtils util,
            RestTemplate restOperation
    ) {
        this.util = util;
        this.restOperation = restOperation;
    }

    private List<StudentDto> response2Students(ResponseEntity<String> responseEntity){
        restOperation.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        try {
            ObjectMapper mapper = new ObjectMapper();
            List<StudentDto> locals = Arrays.asList(mapper.readValue(responseEntity.getBody(), StudentDto[].class));
            return locals;
        }catch (IOException e){
            LOG.debug("IO-err. Fail to read JSON"+ e);
            throw new RuntimeException(e);
        }catch (RuntimeException re){
            LOG.debug("RTE-err. Fail to read JSON"+ re);
            throw re;
        }
    }

    //--------------//
    //Create Student//
    //--------------//
    @HystrixCommand(fallbackMethod = "defaultCreateStudent",commandProperties =
            {@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "60000")})
    public ResponseEntity<StudentDto> createStudent(StudentDto studentDto){
        LOG.info("Will call createStudent with hystrix protection");
        String url = "http://core-service/informations/";
        LOG.debug("createStudent from URL: "+url);

        ResponseEntity<StudentDto> result = restOperation.postForEntity(url, studentDto, StudentDto.class);

        LOG.debug("createStudent http-status: "+result.getStatusCode());
        LOG.debug("createStudent body: "+result.getBody());

        StudentDto studentDtoResult = result.getBody();
        LOG.debug("createStudent.cnt "+studentDtoResult.toString());
        return  util.creatOkResponse(studentDtoResult);
    }
    /**
     * Fallback Method createStudent
     *
     * @return
     */

    public ResponseEntity<StudentDto> defaultCreateStudent(StudentDto studentDto) {
        LOG.debug("Using fallback method for createStudent with name = " +studentDto.getName());
        return util.createResponse(studentDto, HttpStatus.OK);
    }

    //--------------//
    //Get A Student//
    //--------------//
    @HystrixCommand(fallbackMethod = "defaultGetStudent", commandProperties =
            {@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "60000")})
    public ResponseEntity<StudentDto> getStudent(int id){
        LOG.info("Will call getStudent with hystrix protection");
        String url = "http://core-service/informations/"+id;
        LOG.debug("getStudent from URL: "+url);

        ResponseEntity<StudentDto> result = restOperation.getForEntity(url, StudentDto.class);

        LOG.debug("getStudent http-status: "+result.getStatusCode());
        LOG.debug("getStudent body: "+result.getBody());

        StudentDto studentDtoResult = result.getBody();
        LOG.debug("getStudent.cnt "+studentDtoResult.toString());
        return util.creatOkResponse(studentDtoResult);
    }
    /**
     * Fallback Method Get A Student
     *
     * @Return
     */

    public ResponseEntity<StudentDto> defaultGetStudent(int id){
        LOG.debug("Using fallback method for getStuden with id = "+id);
        StudentDto studentDto = new StudentDto();
        studentDto.setName("ERROR");
        return util.createResponse(studentDto, HttpStatus.OK);
    }

    //--------------//
    //Get ALL Student//
    //--------------//

    @HystrixCommand(fallbackMethod = "defaultGetALLStudent", commandProperties =
            {@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "60000")})
    public ResponseEntity<List<StudentDto>> getAllStudent(){
        LOG.info("Will call getAllStudent with hystrix protection");

        String url = "http://core-service/informations/";
        LOG.debug("getAllStudent from URL: "+url);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
        HttpEntity<?> entity = new HttpEntity<>(headers);

        ResponseEntity<String> resultStr = restOperation.exchange(
                builder.build().encode().toUri(),
                HttpMethod.GET,
                entity,
                String.class);

        LOG.debug("getAllStudent http-ststus: "+resultStr.getStatusCode());
        LOG.debug(("getAllStudent body: " +resultStr.getBody()));

        List<StudentDto> studentDtoResult = response2Students(resultStr);
        LOG.debug("getALLStudent.cnt "+ studentDtoResult.toString());
        LOG.debug(studentDtoResult.toString());

        return util.creatOkResponse(studentDtoResult);

    }

    /**
     * FallBack Method get ALlSudent
     *
     * @Return
     */

    public ResponseEntity<List<StudentDto>> defaultGetALlStudent(int age){
        LOG.debug("Using fallback method for getALlStudent");
        return util.createResponse(null, HttpStatus.OK);
    }


    //--------------//
    //Update Student//
    //--------------//

    @HystrixCommand(fallbackMethod = "defaultUpdateStudent", commandProperties =
            {@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "60000")})
    public ResponseEntity<StudentDto> updateStudent(StudentDto studentDto){
        LOG.info("Will call updateStudent hystrix with protection");

        String url = "http://core-service/informations/";
        LOG.debug("updateStudent fro URL: "+url);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<StudentDto> requestUpdate = new HttpEntity<>(studentDto, headers);
        ResponseEntity<StudentDto> resultStr = restOperation.exchange(
                url,
                HttpMethod.PUT,
                requestUpdate,
                StudentDto.class
        );
        StudentDto studentDtoResult = resultStr.getBody();
        return util.creatOkResponse(studentDtoResult);
    }

    /**
     * Fallback method update student
     *
     * @return
     */

    public ResponseEntity<StudentDto> defaultUpdateStudent(StudentDto studentDto){
        LOG.debug("Using fallback method for Update Student with age = "+studentDto.getAge());
        StudentDto studentDtoResult = new StudentDto();
        studentDtoResult.setName("ERROR");
        return util.createResponse(studentDtoResult, HttpStatus.OK);

    }

    @HystrixCommand(fallbackMethod = "defaultDeleteStudent", commandProperties =
            {@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "60000")})
    public ResponseEntity<String> deleteStudent(int id){
        LOG.info("Will call deleteStudent hystrix with protection");

        String url = "http://core-service/informations/"+id;
        LOG.debug("deleteStudent From URL: "+url);

        restOperation.delete(url);

        return util.creatOkResponse(HttpStatus.OK.getReasonPhrase());

    }

    /**
     * Fallback method deleteStudent
     *
     * @return
     */
    public ResponseEntity<String> defaultDeleteStudent(int id){
        LOG.debug("Using fallback method for delete Student with id = "+id);
        return util.createResponse("ERROR", HttpStatus.OK);
    }

}

package com.demotestapi.composite.student;


import org.apache.log4j.BasicConfigurator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableHystrixDashboard
@EnableCircuitBreaker
@EnableEurekaClient
@EnableDiscoveryClient
@ComponentScan({"com.testapi.model.util", "com.demotestapi.composite.student"})
public class StudentCompositeServiceApplication {

    @Bean
    @LoadBalanced
    RestTemplate restTemplate() {return new RestTemplate();}

    public static void main(String[] args) {
        BasicConfigurator.configure();
        ConfigurableApplicationContext context = SpringApplication.run(StudentCompositeServiceApplication.class, args);

    }
}

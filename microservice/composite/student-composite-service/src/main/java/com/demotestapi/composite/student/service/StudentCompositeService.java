package com.demotestapi.composite.student.service;


import com.testapi.model.student.Student;
import com.testapi.model.student.StudentDto;
import com.testapi.model.util.DebugLog;
import com.testapi.model.util.StudentServiceUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@RestController
public class StudentCompositeService {

    private static final Logger LOG = LoggerFactory.getLogger(StudentCompositeService.class);

    private final StudentCompositeIntegration studentCompositeIntegration;
    private final StudentServiceUtils serviceUtils;

    @Autowired
    public StudentCompositeService(StudentCompositeIntegration studentCompositeIntegration, StudentServiceUtils studentServiceUtils){
        this.studentCompositeIntegration = studentCompositeIntegration;
        this.serviceUtils = studentServiceUtils;
    }

    @GetMapping(value = "informations/{id}", produces = "application/json")
    public ResponseEntity<StudentDto> getStudent(@PathVariable int id){
        StudentDto getStudentDtoResult = getBasicStudent(id);
        return serviceUtils.creatOkResponse(getStudentDtoResult);
    }

    @GetMapping(value = "informations", produces = "application/json")
    public ResponseEntity<List<StudentDto>> getAllStudent(){
        List<StudentDto> getAllStudentResult = getAllBasicStudent();
        return serviceUtils.creatOkResponse(getAllStudentResult);
    }

    @PostMapping(value = "informations", produces = "application/json")
    public ResponseEntity<StudentDto> createStudent(@RequestBody StudentDto studentDto){
        StudentDto createStudentResult = createBasicStudent(studentDto);
        return serviceUtils.creatOkResponse(createStudentResult);
    }

    @PutMapping(value = "informations", produces = "application/json")
    public ResponseEntity<StudentDto> updateStudent(@RequestBody StudentDto studentDto){
        StudentDto updateStudentResult = updateBasicStudent(studentDto);
        return serviceUtils.creatOkResponse(updateStudentResult);
    }

    @DeleteMapping(value = "informations/{id}", produces = "application/json")
    public ResponseEntity<String> deleteStudent(@PathVariable("id") int id){
        String deleteStudentResult = deleteBasicStudent(id);
        return serviceUtils.creatOkResponse(deleteStudentResult);
    }

    private StudentDto createBasicStudent(@RequestBody StudentDto studentDto){
        ResponseEntity<StudentDto> responseEntity = studentCompositeIntegration.createStudent(studentDto);
        StudentDto createStudentResult = null;
        if (!responseEntity.getStatusCode().is2xxSuccessful()){
            DebugLog.logMessage("Call to createBasicStudent failed: "+ responseEntity.getStatusCode());
        }else {
            createStudentResult = responseEntity.getBody();
        }
        return createStudentResult;
    }

    private StudentDto getBasicStudent(@PathVariable("id") int id){
        ResponseEntity<StudentDto> responseEntity = studentCompositeIntegration.getStudent(id);
        StudentDto getStudentResult = null;
        if(!responseEntity.getStatusCode().is2xxSuccessful()){
            DebugLog.logMessage("Call to getBasicStudent failed: "+responseEntity.getStatusCode());
        }else {
            getStudentResult = responseEntity.getBody();
        }
        return getStudentResult;
    }

    private List<StudentDto> getAllBasicStudent(){
        ResponseEntity<List<StudentDto>> responseEntity = studentCompositeIntegration.getAllStudent();
        List<StudentDto> getAllStudentResult = null;
        if (!responseEntity.getStatusCode().is2xxSuccessful()){
            DebugLog.logMessage("Call to getAllBasicStudent failed: "+responseEntity.getStatusCode());
        }else {
            getAllStudentResult = responseEntity.getBody();
        }
        return getAllStudentResult;
    }

    private StudentDto updateBasicStudent(StudentDto studentDto){
        ResponseEntity<StudentDto> responseEntity = studentCompositeIntegration.updateStudent(studentDto);
        StudentDto updateStudentResult = null;
        if(!responseEntity.getStatusCode().is2xxSuccessful()){
            DebugLog.logMessage("Call to updateBasicStudent failed: "+responseEntity.getStatusCode());
        }else {
            updateStudentResult = responseEntity.getBody();
        }
        return updateStudentResult;
    }

    private String deleteBasicStudent(int id){
        ResponseEntity<String> responseEntity = studentCompositeIntegration.deleteStudent(id);
        String deleteStudentResult = null;
        if (!responseEntity.getStatusCode().is2xxSuccessful()){
            DebugLog.logMessage("Call to deleteBasicStudent failed: "+responseEntity.getStatusCode());

        }else {
            deleteStudentResult = responseEntity.getBody();
        }
        return deleteStudentResult;
    }

}

package com.demotestapi.core.student.controller;

import com.demotestapi.core.student.service.StudentService;
import com.testapi.model.student.Student;
import com.testapi.model.student.StudentDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
//@RequestMapping("/api")
public class StudentController  {

    @Autowired
    StudentService studentService;


   @GetMapping(value = "/informations", produces = "application/json")
    public ResponseEntity<?> findAllStudent() {
       List<StudentDto> information = studentService.findAllStudent();
           return new ResponseEntity<>(information, HttpStatus.OK);
   }

   @GetMapping(value = "/informations/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
   public ResponseEntity<?> getStudentByID(@PathVariable("id") int id ) {
      StudentDto information = studentService.findByStudentIdConvertToDto(id);
       if(information != null){
           return new ResponseEntity<>(information, HttpStatus.OK);
       }else {
           return new ResponseEntity<>(HttpStatus.NOT_FOUND);
       }
   }

    @RequestMapping(value = "/informations",
        method = RequestMethod.POST,
        produces = "application/json")
    public ResponseEntity<?> createStudent(
            @RequestBody StudentDto studentDto, UriComponentsBuilder builder){
            studentService.create(studentDto);
       return new ResponseEntity<>(studentDto, HttpStatus.CREATED);
   }

   @PutMapping(value = "/informations", produces = "application/json")
    public ResponseEntity<?> updateStudent(
            @RequestBody StudentDto studentDto){
      StudentDto currentStudent = studentService.update(studentDto);
       if(currentStudent==null){
           return new ResponseEntity<>(HttpStatus.NO_CONTENT);
       }else {
           currentStudent.setName(studentDto.getName());
           currentStudent.setAge(studentDto.getAge());
           studentService.update(studentDto);
           return new ResponseEntity<>(currentStudent,HttpStatus.OK);
       }
   }

   @DeleteMapping(value = "/informations/{id}", produces = "application/json")
    public ResponseEntity<?> deleteStudent(
            @PathVariable("id") int id) {
            Student information = studentService.findByID(id);
       if (information == null) {
           return new ResponseEntity<>(HttpStatus.NO_CONTENT);
       }else {
           studentService.delete(id);
           return new ResponseEntity<>(HttpStatus.OK.getReasonPhrase(),HttpStatus.OK);
       }
   }

}


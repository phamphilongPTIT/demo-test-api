package com.demotestapi.core.student.service;

import com.testapi.model.student.Student;
import com.testapi.model.student.StudentDto;

import java.util.List;

public interface StudentService {
    StudentDto create(StudentDto studentDto);

    StudentDto findByStudentIdConvertToDto(int id);

    List<StudentDto> findAllStudent();

    Student findByID(int id);

    StudentDto update(StudentDto studentDto);

    StudentDto delete(int id);

}

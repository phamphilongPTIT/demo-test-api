package com.demotestapi.core.student.service;

import com.demotestapi.core.student.respository.StudentRepository;
import com.testapi.model.student.Student;
import com.testapi.model.student.StudentDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;

import javax.transaction.Transactional;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@Transactional
public class StudentServiceIml implements StudentService {

    @Autowired
     StudentRepository studentRepository;


    @Override
    @Cacheable(value = "menuListStudent")
    public List<StudentDto> findAllStudent() {
        List<Student> studentList = studentRepository.findAll();
        return convertToDTOs(studentList);
    }

    @Override
    public Student findByID(int id) {
        Student result = studentRepository.findById(id).orElse(null);
        return result;
    }

    @Override
    @GetMapping(value = "/{id}")
    @Cacheable(value = "singleStudent", key = "#id")
    public StudentDto findByStudentIdConvertToDto(int id) {
        Student result = studentRepository.findById(id).orElse(null);
        return convertToDTO(result);
    }


    @Override
    @Cacheable(value = "singleStudent", key = "#studentDto.id")
    @CachePut(value = "singleStudent", key = "#studentDto.id")
    @PutMapping(value = "/{id}")
    public StudentDto create(StudentDto studentDto) {
        Student persisted = Student.builder()
                .name(studentDto.getName())
                .age(studentDto.getAge())
                .build();
        persisted = studentRepository.save(persisted);
        return convertToDTO(persisted);
    }

    @Override
    @Cacheable(value = "singleStudent", key = "#studentDto.id")
    @CachePut(value = "singleStudent", key = "#studentDto.id")
    @PutMapping(value = "/{id}")
    public StudentDto update(StudentDto studentDto) {
        Student updated = findByID(studentDto.getId());
        if(updated !=null) {
            updated.setId(studentDto.getId());
            updated.setName(studentDto.getName());
            updated.setAge((studentDto.getAge()));
            studentRepository.saveAndFlush(updated);
        }
        return convertToDTO(updated);
    }



    @Override
    @Caching(
            evict = {
                  @CacheEvict(value = "singleStudent", key = "#id"),
                    @CacheEvict(value = "menuListStudent", allEntries = true)
            })
    public StudentDto delete(int id) {
        Student student = findByID(id);
        if(student != null) {
            studentRepository.delete(student);
            return convertToDTO(student);
        }else return null;
    }

    private List<StudentDto> convertToDTOs(List<Student> students) {
        if (students != null) {
            return students.stream().map(this::convertToDTO).collect(toList());
        }else return null;
    }

    private StudentDto convertToDTO(Student student){
        StudentDto dto = new StudentDto();
        if(student !=null){
            dto.setId(student.getId());
            dto.setName((student.getName()));
            dto.setAge((student.getAge()));
            return dto;
        }else return null;
    }



}

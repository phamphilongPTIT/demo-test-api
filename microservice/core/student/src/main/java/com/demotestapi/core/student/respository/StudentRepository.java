package com.demotestapi.core.student.respository;


import com.testapi.model.student.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {

    Optional<Student> findById(int id);
    Student save(Student student);
    void delete(Student student);

       List<Student> findAll();


}

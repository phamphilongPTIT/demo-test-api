package com.demotestapi.core.student;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EntityScan("com.testapi.model.student")
@EnableCaching
@EnableEurekaClient
public class TestApplication {
    public static void main(String[] args) { SpringApplication.run(TestApplication.class,args);

    }

}
